package socialmedia;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Whatsappmeet {

    public static void main(String[] args) throws InterruptedException {

        GregorianCalendar time=new GregorianCalendar();
        int hour=time.get(Calendar.HOUR_OF_DAY);
        System.setProperty("webdriver.chrome.driver","src/main/java/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://web.whatsapp.com/");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60000));
        driver.findElement(By.className("_13NKt")).click();
        driver.findElement(By.className("_13NKt")).sendKeys("ma");
        Thread.sleep(6000);
        WebElement contactxat=driver.findElement(By.xpath("//*[@id=\"pane-side\"]/div[1]/div/div/div[3]/div/div/div[2]/div[1]/div[1]/span/span"));
        contactxat.click();
        WebElement textbox1= driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]"));
        if (hour<12){
            textbox1.sendKeys("Good Morning!!!");
        }
        else if (hour>12 && hour<17){
            textbox1.sendKeys("Good Afternoon!!!");
        }
        else {
            textbox1.sendKeys("Good Evening!!!");
        }
        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Here is an important announcement regarding meeting availability for you.");
        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Type 1"+"\n"+"type 2");
        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]")).click();
        WebDriverWait wait=new WebDriverWait(driver,Duration.ofSeconds(20));
        WebElement reply=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")));
        String a=new String(reply.getText());
        int k=Integer.parseInt(a);
        if (k==1){

            textbox1.sendKeys("Meeting will be sheduled");
        }

        else if (k==2){

            textbox1.sendKeys("Meeting is canceled");
        }

        else {

            textbox1.sendKeys("Typing error");
        }


    }

}
